{
 "@context": "http://schema.org",
 "@type": "Event",
 "name": "Animarathon XVI",
 "startDate": "2018-02-10T9:00",
 "doorTime": "2018-02-10T9:00",
 "endDate": "2018-02-11T17:30",
 "url": "https://animarathon.com/animarathon.html",
 "image": "https://animarathon.com/media/media_nonfree/sisters.png",
 "typicalAgeRange": "Everyone",
 "location": [{
  "@type": "Place",
  "sameAs": "https://www.bgsu.edu/bowen-thompson-student-union.html",
  "name": "The Bowen Thompson Student Union",
  "address": [{
   "streetAddress": "Bowen Thompson Student Union, Bowling Green State University", 
   "addressLocality": "Bowling Green", 
   "addressRegion": "OH",
   "postalCode": "43404"
   }]
 }]

}
